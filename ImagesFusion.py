#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 11:22:28 2018

@author: Ismael Chouraqi
"""

"""
    This function use the OpenCV library
    for Linux users you can have it by writting in a terminal :
        sudo apt-get install python-opencv
"""
import numpy as np
import scipy.signal as sp
import os
try:
    import cv2
except ImportError:
    print("""
          You need to download the library called OpenCV
          for Linux operating system, in a terminal : sudo apt-get install python-opencv
          """)
    
def createFolder(Path):
    try:
        if not os.path.exists(Path):
            os.makedirs(Path)
    except OSError:
        print ('Error: Creating directory. ' + Path)

def Display (Name, Var):
    cv2.namedWindow(Name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(Name, 700, 700)
    cv2.imshow(Name, Var)
    cv2.waitKey(0)
    cv2.destroyWindow(Name)
    
def LaplacianPyramid_down(Image, height):
    Pyramid = []
    Base = Image.copy()
    for i in range(height):
        temp = cv2.pyrDown(Base)
        top = cv2.pyrUp(temp , dstsize = (Base.shape[1], Base.shape[0]))
        Pyramid.append(cv2.subtract(Base, top))
        Base = temp
    Pyramid.append(temp)
    return Pyramid

def GaussianPyramid_down(Image, height):
    Pyramid = []
    Base = Image.copy()
    for i in range(height):
        Pyramid.append(Base)
        Base = cv2.pyrDown(Base)
    Pyramid.append(Base)
    return Pyramid
    
def LaplacianPyramid_up(Pyramid):
    Base = list(Pyramid)
    Image = Base[-1]
    for i in range(2, len(Pyramid)+1):
        Image = Base[-i] + cv2.pyrUp(Image, dstsize = (Base[-i].shape[1],Base[-i].shape[0]) )
    return Image

def _Norm(Image):
    Image[Image<0] = 0
    Image[Image>1] = 1
    return Image
    
def Laplacien_absolute(Img):
    Img_g = cv2.cvtColor(Img, cv2.COLOR_BGR2GRAY)
    Lap = np.abs(sp.convolve2d(Img_g, np.reshape([0, 1, 0, 1, -4, 1, 0, 1, 0], [3,3]), mode = 'same'))
    print ("-----Laplacian")
    return Lap

def RGB_st_deviation(Img):
    St_dev = np.std(Img, axis = 2)
    print ("-----Standard Deviation")
    return St_dev

def WellExposedness(Img_in):
    WE_RGB = np.zeros(Img_in.shape)
    Img  = Img_in.copy()
    sigma = 0.2
    for c in range(3):
        WE_RGB[:,:, c] = np.exp(-(Img[:,:,c] - 0.5 )**2*1./(2*sigma**2))
    WE = WE_RGB[:,:,0] * WE_RGB[:,:,1] * WE_RGB[:,:,2]
    print ("-----Well exposedness")
    return WE
    
def Mask3Chanel (Mask):
    Mask3 = np.zeros([Mask.shape[0], Mask.shape[1], 3])
    for c in range(3):
        Mask3[:, :, c] = Mask
    return Mask3

def BestExposureMask(Image):
    Mask = Laplacien_absolute(Image) * RGB_st_deviation(Image) * WellExposedness(Image) + 0.000001
    return Mask

def PyrMask(Images):
    depth = int(np.log(np.minimum(Images[0].shape[0], Images[0].shape[1]) - 2))+1
    pyrMask = [GaussianPyramid_down(BestExposureMask(i), depth) for i in Images]
    pyrMask = pyrMask / (np.sum(pyrMask, 0) )
    return pyrMask
    
def PyrImages(Photos):
    print("Pyramids Fusion, please wait...")
    depth = int(np.log(np.minimum(Photos[0].shape[0], Photos[0].shape[1]) - 2))+1
    return [LaplacianPyramid_down(i, depth) for i in Photos]
        

def ImageFusion(Pyr_i, Pyr_m):
    print("Image construction...")
    Pyramids = [[Pyr_i[i][j] * Mask3Chanel(Pyr_m[i][j]) for j in range(np.shape(Pyr_i)[1]) ] for i in range(np.shape(Pyr_i)[0])]
    PyrSum = [sum(i) for i in zip(*Pyramids)]
    return LaplacianPyramid_up(PyrSum)

def ContrastSaturations(Image, alpha = 6, Nb = 5 ):
    Img = np.copy(Image)
    if Image.dtype == 'uint8':
        Img = Img.astype(np.float32)/255
    Images = []
    if Nb < 2 :
        raise ValueError("the third item called Nb must be greater than 1")
    k_min = -Nb//2
    for k in np.arange(k_min, -k_min+1):
        if k >=0:
            Images.append(np.maximum(0, 6**(k*1./(Nb-1))*(Img-1) + 1))
        else:
            Images.append(np.minimum(1, 6**(-k*1./(Nb-1))*Img))
    return Images
    
    
    
    
    

if __name__ == "__main__" :
    print("======= Image Fusion Process =======")
    createFolder('Results/')
    DOWN_SAMPLING = True
    TEST = ['MultiExposure_Fusion', 'SingleExposure_Fusion']
    
    Paths = ["Lampe_marakchi/L%d.JPG" % (i) for i in 
            range(4)]
    
    Images = [cv2.normalize(cv2.imread(i), None, alpha = 0, beta = 1, norm_type = cv2.NORM_MINMAX, dtype = cv2.CV_32F) for i in Paths]
    for Test in TEST:
        if Test == 'SingleExposure_Fusion':
            Images = ContrastSaturations(Images[0])
        if DOWN_SAMPLING:
            Images = [cv2.resize(i, (Images[0].shape[1]/2, Images[0].shape[0]/2)) for i in Images]
        
            # Saving down sampled Pictures for scaled comparison
            for i in range(4):
                cv2.imwrite("Results/L_{}_{}.jpg".format(Test, i) , np.uint8(Images[i]*255))
    
            
        # visualisation        
        Pyr_m = PyrMask(Images)
        Pyr_i = PyrImages(Images)
        _Photo = ImageFusion(Pyr_i, Pyr_m)
        Display('Result', _Photo )
        cv2.imwrite("Results/Res_{}.jpg".format(Test), np.uint8(_Norm(_Photo)*255))
