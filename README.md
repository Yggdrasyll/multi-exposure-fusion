# Exposure Fusion implementation in Python

I made this script for personal use, and also to train. If you use this code or if this code was useful to you, do not forget to mention it in your contributions.

## Getting Started


### Prerequisites

* Numpy
* scipy
* Os
* OpenCv

For Windows users, you will need to change '/' by '\\'.

### Running the tests

Run the ImagesFusion.py script in a terminal
```
python ImagesFusion.py
```

## Built With

* [Python](https://www.python.org/downloads/) 
* [OpenCV](https://github.com/opencv/opencv)


## Authors

* **Ismaël Chouraqi** - *Ph.D at université Paris Descartes - MAP5* - [Gitlab](https://gitlab.com/Yggdrasyll)


## Acknowledgments

* Charles Hessel [IPOL](http://www.ipol.im/pub/pre/230/)
* Tom Mertens, Jan Kautz, Frank Van Reeth [PDF](http://cholla.mmto.org/minerals/macro/exposure_fusion_reduced.pdf)


